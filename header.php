<!doctype html>
<html lang="hu">
   <head>
      <meta content="IE=9" http-equiv="X-UA-Compatible" />
	  <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	  <meta content="no" http-equiv="imagetoolbar" />
	  <meta content="Vermis" name="author" />
	  <meta content="A Vermis Production egy online ügynökség, melynek specialitása az online PR, a terjesztés és a közösségek aktivizálása. Kattintson ide és ismerje meg megoldásainkat!" name="description" />
	  <meta content="Vermis" name="reply-to" />
	  <meta content="Copyright © 2013 Vermis. " name="copyright" />
      <title>Vermis Production</title>
      <link href="img/icon/favicon.png" rel="shortcut icon" type="image/icon">
		
      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="css/animate.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <link href="css/magnific-popup.css" rel="stylesheet">
      <link href="css/flexslider.css" rel="stylesheet">
      <link href="css/owl.carousel.css" rel="stylesheet">
      <link href="css/owl.theme.default.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
      <link href="css/theme-corporate-2.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
	  <link rel="stylesheet" type="text/css" href="css/jquery.cookiebar.css" />
	  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	  <script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#ffffff",
      "text": "#ffc300"
    },
    "button": {
      "background": "transparent",
      "text": "#ffc300",
      "border": "#ffc300"
    }
  },
  "showLink": false,
  "content": {
    "message": "A sütik alapvető fontosságúak ahhoz, hogy a legtöbbet tudja kihozni a weboldalunkból. Szinte minden weboldal használja őket. A sütik az Ön preferenciáit és egyéb olyan információkat tárolnak, melyek segítenek hatékonyabbá tenni a weboldalunkat – de ne aggódjon, az oldalon keresztül semmi olyan információt nem gyűjtünk Önről, mely alapján személye beazonosítható lenne!",
    "dismiss": "Elfogad"
  }
})});
</script>  
      
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '854056204647679');
        <?php
        if($_GET['page']=="koszonjuk")
        {
            echo "fbq('track', 'Lead');";
        }
        elseif($_GET['page']=="megerosites")
        {
            echo "fbq('track', 'CompleteRegistration');";
        }
        else {
            echo "fbq('track', 'PageView');";

        }
        ?>
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=854056204647679&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->    
<style>
	@import url('https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700&display=swap');
</style>
   </head>
   
   <body>
   <div class="spartan">
      <div class="page-loading">
         <div class="loader"></div> 
         <span class="text">Betöltés...</span> 
      </div>
      <div id="left"></div>
      <div id="right"></div>
      <div id="top"></div>
      <div id="bottom"></div>
      
      <!--NAVBAR
 ================================================== --> 
      <!-- Navigation -->
	  
<?php include ('navigation.php');?>
      <!--<div class="form-search">
         <button type="button" class="close">
         </button>
         <form>
            <input type="search" value="" placeholder="Enter your text" />
            <button type="submit" class="btn">
               <i class="fa fa-search margin-r-5" aria-hidden="true"></i>
               search
            </button>
         </form>
      </div>-->