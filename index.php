<?php include ('header.php');?>
      <div class="theme-corporate-2">
	    <div class="hidden-xs nav-parallax">
         <div class="nav-dots-parallax">
            <ul>
               <li> <a class="active" href="#bg-img">A VERMIS EGY ONLINE REKLÁMÜGYNÖKSÉG. </a></li>
               <li><a href="#strategia">STRATÉGIÁTÓL A KAMPÁNYOKIG</a></li>
               <li><a href="#kampany">A KAMPÁNYTERVEZÉSTŐL A GYÁRTÁSIG. </a></li>
               <li><a href="#mit-csinalunk">MIT CSINÁLUNK</a></li>
               <li><a href="#munkainkrol">MUNKÁINKRÓL</a></li>
               <li><a href="#cikkeink">CIKKEINK</a></li>
			   <li><a href="#kapcsolat">KAPCSOLAT</a></li>
            </ul>
         </div>
      </div>
         <section class="owl-1-full-screen" id="bg-img">
            <div class="parallax-bg overlay-dark owl-content"
               data-center="background-position: 50% 0px;" 
               data-top-bottom="background-position: 50% -150px;" 
               data-anchor-target="#bg-img">
               <div class="owl-content-des-l header-tit">
			  <div class="bg01_f"><img src="img/bg/bg01.png" class="img-responsive center-block"></div>
                  <div class="animated-txt-1 carousel1">					
                     <h1>A VERMIS EGY ONLINE<br>
                         REKLÁMÜGYNÖKSÉG.
                     </h1>
                  </div>
                  <div class="animated-txt-2 carousel2">
                     <p class="animated-txt-2">Márkákat kezelünk a digitális térben, a márkaépítésről,<br>a közösségi téren át, az értékesítés növeléséig.</p>
                  </div>
				  <div class="animated-txt-scroll"><span class="animated-txt-scroll">Görgessen lefelé</span><img src="img/scroll.png" class="img-responsive center-block"></div>
               </div>
            </div>
         </section>
         <section class="service-7 bg-6">
            <div class="container">
               <div class="row">
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">                             
                                 <img src="img/deichmann-logo.png" class="img-responsive center-block">                             
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/nebih-logo.png" class="img-responsive center-block">  
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-delay="0.2s" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/ford-logo.png" class="img-responsive center-block" >  
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-delay="0.4s" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/vatera-logo.png" class="img-responsive center-block">  
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="owl-2-full-screen" id="strategia">
            <div class="parallax-bg overlay-dark2 owl-content"
               data-center="background-position: 50% 0px;" 
               data-top-bottom="background-position: 50% -150px;" 
               data-anchor-target="#bg-img">
               <div class="owl-content-des-l header-tit_2">			  
                  <div class="animated-txt-1 carousel3">					
                     <h1>STRATÉGIÁTÓL A KAMPÁNYOKIG</h1>
                  </div>
                  <div class="animated-txt-2 carousel4">
                     <p class="animated-txt-2">Vagy a meglévő márka stratégiáktól az online kampány<br>taktikai szintjéig elkísérjük ügyfeleinket.</p>
                  </div>
				  <div class="bg02_f stage"><img src="img/bg/bg02.png" class="img-responsive center-block bg02_style"></div>
               </div>
            </div>
         </section>
         <section class="service-7 bg-6-red">
            <div class="container">
               <div class="row">
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">                             
                                 <img src="img/elmu-logo.png" class="img-responsive center-block">                             
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/mall-logo.png" class="img-responsive center-block">  
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-delay="0.2s" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/stihl-logo.png" class="img-responsive center-block" >  
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-delay="0.4s" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/g4s-logo.png" class="img-responsive center-block">  
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="owl-3-full-screen" id="kampany">
            <div class="parallax-bg overlay-dark3 owl-content"
               data-center="background-position: 50% 0px;" 
               data-top-bottom="background-position: 50% -150px;" 
               data-anchor-target="#bg-img">
               <div class="owl-content-des-l header-tit_3">			  
                  <div class="animated-txt-1 carousel5">					
                     <h1>A KAMPÁNYTERVEZÉSTŐL<br> A GYÁRTÁSIG.
                     </h1>
                  </div>
                  <div class="animated-txt-2 carousel6">
                     <p class="animated-txt-2">A digitális aktivitások tervezésétől, az egyes kommunikációs<br> elemek legyártásán át, azok publikációjáig,<br> terjesztésig terjednek kompetenciánk.</p>
                  </div>
				  <div class="bg03_f"><img src="img/bg/bg03.png" class="img-responsive center-block"></div>
               </div>
            </div>
         </section>
         <section class="service-7 bg-6-blue">
            <div class="container">
               <div class="row">
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">                             
                                 <img src="img/windows-logo.png" class="img-responsive center-block">                             
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/bab-logo.png" class="img-responsive center-block">  
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-delay="0.2s" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/cronos-logo.png" class="img-responsive center-block" >  
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="hidden-xs col-md-3 col-sm-3 no-pad wow fadeIn" data-wow-delay="0.4s" data-wow-offset="110">
                     <div class="box-u">
                        <div class="h-center">
                           <div class="v-center">
								<img src="img/tutti-logo.png" class="img-responsive center-block">  
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="service-6" id="mit-csinalunk" style="background-color: #ffe116;">
            <div class="container-fluid">
               <div class="text-center service-wrap wow fadeIn" >
                  <div class="row equal b-w">
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/online-kampanyok-kidolgozasa.png" class="img-responsive munkaink01">
                        </div>
                        <div class="service-info">
                           <h3>ONLINE KAMPÁNYOK<br>KIDOLGOZÁSA</h3>
                           <p>Csapatunk célcsoportspecifikus online kampányok kidolgozásával támogatja a teljes online stratégiát. Kampányainkra komplex mérési taktikát készítünk, az adatokat elemezve pedig visszaforgatunk minden tudást a következő koncepciókba.</p>
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/markastrategia-kidolgozasa.png" class="img-responsive munkaink02">
                        </div>
                        <div class="service-info">
                           <h3>MÁRKASTRATÉGIA<br>KIDOLGOZÁSA</h3>
                           <p>A márkastratégia kidolgozás célja, hogy tudatossá tegye cége bemutatkozását, képviselt értékeit, stílusát.</p>
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/responsive-es-mobile-firendly-fejlesztesek.png" class="img-responsive munkaink01">
                        </div>
                        <div class="service-info">
                           <h3>RESPONSIVE ÉS MOBILE<br>FRIENDLY FEJLESZTÉSEK</h3>
                           <p>Egyedi megjelenésű, reszponzív weboldal készítés korszerű megoldásokkal, amelyek mobilon is tökéletes felhasználói élményt biztosítanak.</p>
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/influencer_es_markanagykoveti_aktivitasok.png" class="img-responsive munkaink03">
                        </div>
                        <div class="service-info">
                           <h3>INFLUENCER ÉS MÁRKA-<br>NAGYKÖVETI AKTIVITÁSOK</h3>
                           <p>Elsődleges szempont számunkra az együttműködéseknél, hogy valóban releváns és hiteles influencereket és lehetséges márkanagyköveteket javasoljunk a márkák számára, mindezt minden esetben insight -és profilkutatásokra alapozzuk.</p>
                        </div>
                     </div>
				  </div>
				  <div class="row equal b-w-b">
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/ppc_adwords_kampanyok.png" class="img-responsive munkaink03">
                        </div>
                        <div class="service-info">
                           <h3>PPC / ADWORDS KAMPÁNYOK</h3>
                           <p>PPC / ADWORDS kampányok építése és optimalizálása, valamint a meghirdetett webhely hatékonyságának növelése. Szinte nincs olyan iparág, piac vagy üzleti helyzet, amelyben ne rendelkeznénk tapasztalattal.</p>
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/social_media_management.png" class="img-responsive munkaink03">
                        </div>
                        <div class="service-info">
                           <h3>SOCIAL MEDIA MANAGEMENT</h3>
                           <p>Ismerjük a hazai és nemzetközi trendeket, elképesztő mennyiségű tartalmat gyártunk és hiszünk a közösség erejében. Nem egyet építettünk már fel és szelidítettünk meg, velük éljünk a mindennapjaink.</p>
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/ux_es_ui_design.png" class="img-responsive munkaink02">
                        </div>
                        <div class="service-info">
                           <h3>UX ÉS UI DESIGN</h3>
                           <p>Hiszünk a letisztult, minimalista felületekben. Egy weboldalnak szépnek, gyorsan betöltődőnek és használhatónak kell lennie. Egyedi webes arculat kialakítása és tervezése, a modern kor elvárásainak megfelelően. </p>
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 job_hover">
                        <div class="icon">
                           <img src="img/munkaink/online_aktivitasok.png" class="img-responsive munkaink03">
                        </div>
                        <div class="service-info">
                           <h3>ONLINE AKTIVITÁSOK</h3>
                           <p>Az engagement fenntartása és növelése céljából folyamatos online aktivitásokkal mozgatjuk meg a közösségeket, szem előtt tartva a platformok kimondott és kimondatlan szabályait és a netikett előírásait.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
		 
<!-- JOBS -->
         <section class="service-7 bg-6-munkaink" id="munkainkrol">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 -md-12 col-sm-12 no-pad wow fadeIn" data-wow-offset="110">
                    <h1 class="product_line_03">MUNKÁINKRÓL</h1>                             
                  </div>	
					<div class="col-md-6 munkaink-right">
						<img src="img/vatera_logo.png" alt="Vatera" class="img-responsive munkaink-img-right">
						<h1 class="product_line_05_right">Közösségi kommunikáció</h1>
						<p>A Vermis 2004-óta foglalkozik a különböző jellegű közösségek menedzselésével, kezdve a Facebooktól a fórumokon és a márkanagykövetek létrehozásán át az influencer marketing platformokig.</p>	
						<a href="/munkainkrol/kozosseg" target="_blank"><button class="kap-but">TOVÁBB</button></a>
					</div>
					<div class="col-md-6 munkaink-left">
						<img src="img/elmu_logo.png" alt="Elmű" class="img-responsive munkaink-img-left">
						<h1 class="product_line_05_left">PPC és média feladatok</h1>
						<p class="hidden-xs_2">Performancia Maketing csapatunk<br>ROI+ kampányok megvalósításán dolgozik,<br>valamint támogatja a digitális stratégiai<br>csapat munkáját.</p>
						<p class="visible-xs_2">Performancia Maketing csapatunk ROI+ kampányok megvalósításán dolgozik, valamint támogatja a digitális stratégiai csapat munkáját.</p>						
						<a href="/munkainkrol/ppc" target="_blank"><button class="kap-but">TOVÁBB</button></a>
					</div>
				</div>
				<div class="col-xs-12 -md-12 col-sm-12 no-pad wow fadeIn empty_space" data-wow-offset="110"></div>
				<div class="row">
					<div class="col-md-6 munkaink-right">
						<img src="img/windows_logo.png" alt="Windows 8" class="img-responsive munkaink-img-right">
						<h1 class="product_line_05_right">Influencer Marketing</h1>
						<p>Az Influencer Marketing az egyik legdinamikusabban fejlődő területe a korszerű marketing kommunikációnak. Ismerje meg esettanulmányainkat.</p>
						<a href="/munkainkrol/influencer_marketing" target="_blank"><button class="kap-but">TOVÁBB</button></a>
					</div>
					<div class="col-md-6 munkaink-left">
						<img src="img/babszinhaz_logo.png" alt="Budapest Bábszínház" class="img-responsive munkaink-img-left">
						<h1 class="product_line_05_left">Web-fejlesztés</h1>
						<p>Csapatunk egyik kiemelt szakmai területe az egyedi webes fejlesztések. Webfejlesztő csapatunk backend / frontend / PM és minőségbiztosítás területét öleli fel. website-tol a bannerig.</p>						
						<a href="/munkainkrol/wordpress_fejlesztes" target="_blank"><button class="kap-but">TOVÁBB</button></a>
					</div>
                  <div class="col-xs-12 -md-12 col-sm-12 no-pad wow fadeIn" data-wow-offset="110" style="margin:40px;"></div>
               </div>
            </div>
         </section>		 
		 
		 <!-- NEWS -->
		 
         <section class="service-7" id="cikkeink">
            <div class="container-fluid">
               <div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 main_bg-1"></div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cikkeink-left">
						<p class="cikkeink_bread_left">Cikkeink /</p>
						<h1 class="product_line_06_left">A tudás nincs ingyen</h1>
						<p>Nincs ingyen média és nincs ingyen információ se. Még akkor sem, ha nem került Neked semmibe. Jobb, ha megszokod – állítja Bánki Attila, a Vermis Production alapító tulajdonosa és ügyvezetője.</p>				
						<a href="http://www.digitalhungary.hu/media/A-tudas-nincs-ingyen/2698/" target="_blank"><button class="kap-butv2">TOVÁBB</button></a>
					</div>
               </div>
			   <div class="row reorder-xs"> 
			   		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cikkeink-right">
        				<p class="cikkeink_bread_right">Cikkeink /</p>
						<h1 class="product_line_06_right">12 performance marketing hiba...</h1>
						<p>Egyre többen nyúlunk a performancia marketinghez, így érdemes áttekinteni, melyek azok a hibák, melyeket mindenképpen el kell kerülni.</p>
						<a href="http://www.digitalhungary.hu/marketing/12-performance-marketing-hiba-amit-muszaj-elkerulnod/1138/" target="_blank"><button class="kap-butv2">TOVÁBB</button></a>
					</div>
 			   		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 main_bg-2"></div>   
				</div>
               <div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 main_bg-3"></div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cikkeink-left">
						<p class="cikkeink_bread_left">Cikkeink /</p>
						<h1 class="product_line_06_left">Hivatkozások ereje. Felértékelődtek a fogyasztói nagykövetek.</h1>
						<p>Tudta Ön, hogy a márkáink nagykövetei akkor is képviselnek minket, ha nem költünk rájuk? Akkor is „velünk vannak”, ha éppen nincs kampányidőszak?</p>	
						<a href="http://www.digitalhungary.hu/marketing/Hivatkozasok-ereje-Felertekelodtek-a-fogyasztoi-nagykovetek/2021/" target="_blank"><button class="kap-butv2">TOVÁBB</button></a>
					</div>
               </div>
			   <div class="row reorder-xs"> 
			   		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 cikkeink-right">
        				<p class="cikkeink_bread_right">Cikkeink /</p>
						<h1 class="product_line_06_right">Az a szó, hogy „egészséges”</h1>
						<p>A legtöbb életminőséggel foglalkozó iparág elkövet egy hosszú távon igazán fajsúlyos kommunikációs hibát. A fogyasztói tudatlanság mentén devalvál olyan értékeket, mint az „egészséges”...</p>						
						<a href="https://www.digitalhungary.hu/media/Az-a-szo-hogy-egeszseges-nem-jelent-semmit/3028/" target="_blank"><button class="kap-butv2">TOVÁBB</button></a>
					</div>
 			   		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 main_bg-4"></div>   
				</div>
            </div>
         </section>
<?php include ('footer.php');?>
   </body>
</html>
