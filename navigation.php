      <nav class="navbar navbar-fixed-top nav-dark nav-shrink" role="navigation">
         <div class="container">		 
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <img src="https://<?php echo $_SERVER['HTTP_HOST']; ?>/img/icon/toggle-light.png" width="32" height="32" alt=""/>
               </button>
			    <div class="hidden-sm hidden-md hidden-lg"><a class="navbar-brand" href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/">
                  <img src="https://<?php echo $_SERVER['HTTP_HOST']; ?>/img/vermis_logo.png" class="img-responsive" alt=""/>
               </a></div>
            </div>
            <div class="collapse navbar-collapse">
               <ul class="nav navbar-nav">
			   <li class="disp-no pad-menu-logo"><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/"><img src="https://<?php echo $_SERVER['HTTP_HOST']; ?>/img/vermis_logo.png" class="img-responsive" alt=""/></a></li>
                   <li class="dropdown menu-large pad-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="navbar_style">Magunkról</div><b class="caret-arrow"></b></a>
                     <ul class="dropdown-menu megamenu row ">
                        <li class="col-sm-12">
                           <ul class="list-inline">                            
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkamodszer">Munkamódszer</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/a-csapat">A csapat</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/cikkeink">Cikkeink</a></li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                 <li class="dropdown menu-large pad-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="navbar_style">Munkáinkról</div><b class="caret-arrow"></b></a>
                     <ul class="dropdown-menu megamenu row">
                        <li class="col-sm-12">
                           <ul class="list-inline" style="padding-left:0px;">     
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/performancia-marketing">Performancia marketing</a></li>						   
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/strategia">Stratégia</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/arculat">Arculat</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/ux">UX</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/gui">GUI</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/wordpress_fejlesztes">Wordpress fejlesztés</a></li>
							  <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/magento_fejlesztes">Magento fejlesztés</a></li> 
                           </ul>
                        </li>
						<li class="col-sm-12">
                           <ul class="list-inline" style="padding-left:0px;">                       
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/egyedi_fejlesztes">Egyedi fejlesztés</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/mobile_fejlesztes">Mobile fejlesztés</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/ppc">PPC feladatok</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/kozosseg">Közösségi kommunikáció</a></li>
                              <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/influencer_marketing">Influencer marketing</a></li>
							  <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/digital_pr">Digital PR</a></li>
							  <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/munkainkrol/hr">HR</a></li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                 <li class="dropdown menu-large pad-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><div class="navbar_style">Workshopjaink</div><b class="caret-arrow"></b></a>
                     <ul class="dropdown-menu megamenu row">
                        <li class="col-sm-12">
                           <ul class="list-inline" style="padding-left:0px;">
						   <li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/social-media-workshop/2019_augusztus/">Social Media Inspiratív Workshop - 2019. Augusztus</a></li>	
							<li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/social-media-workshop/2019_junius/">Social Media Workshop - 2019. Június</a></li>	
							<li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/influencer/">Influencer Reboot Workshop - 2018. Február</a></li>
							<li><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/development_workshop/">Development Workshop - 2018. Április</a></li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                  <li class="pad-menu"><a href="https://<?php echo $_SERVER['HTTP_HOST']; ?>/kapcsolat"><div class="navbar_style">Kapcsolat</div><b class="caret-arrow"></b></a></li>
				  <li class="pad-menu" style="padding-top: 42px;padding-bottom: 34px;"><a href="https://www.facebook.com/vermis.production/?fref=ts" target="_blank" style="display: inline;list-style-type: none;"><i class="fab fa-facebook-f fa-2x"></i></a>&nbsp;&nbsp;<a href="https://www.youtube.com/channel/UCkK_nDEmZ0gnEJ0mA5vJPVA" target="_blank" style="display: inline;list-style-type: none;"><i class="fab fa-youtube fa-2x"></i></a></li>
				  
				  
			

                  <!--<li class="dropdown menu-large"> <a href=".form-search">
                        <i class="fa fa-search" aria-hidden="true"></i>
                     </a></li>-->
               </ul>
            </div>

			</div>
         </div></div>
      </nav>