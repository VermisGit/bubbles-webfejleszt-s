<?php

// configure
$from = 'your-site@gmail.com'; 
$sendTo = 'your-site@gmail.com';
$subject = 'New message from contact form';
$fields = array('subject' => 'Subject', 'name' => 'Name', 'person' => 'Person', 'company' => 'Company', 'date' => 'Date', 'phone-number' => 'Phone Number', 'email' => 'Email', 'message' => 'Message', 'comments' => 'Comments', 'newsletter' => 'Newsletter', 'lastname' => 'Last Name'); // array variable name => Text to appear in email
$okMessage = 'Contact form successfully submitted. Thank you, I will get back to you soon!';
$errorMessage = 'There was an error while submitting the form. Please try again later';

// let's do the sending

try
{
    $emailText = "You have new message from contact form\n=============================\n";

    foreach ($_POST as $key => $value) {

        if (isset($fields[$key])) {
            $emailText .= "$fields[$key]: $value\n";
        }
    }

    mail($sendTo, $subject, $emailText, "From: " . $from);

    $responseArray = array('type' => 'success', 'message' => $okMessage);
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);
    
    header('Content-Type: application/json');
    
    echo $encoded;
}
else {
    echo $responseArray['message'];
}
