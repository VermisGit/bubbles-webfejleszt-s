         <section class="bg-3 pad-m-t-50 pad-m-b-0">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12">
						<div class='content-container  embed-container  maps'style="-webkit-filter: grayscale(100%); filter: grayscale(100%);">
					<div id="map"></div>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2696.415886416649!2d19.07538811586844!3d47.48181100458285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dcfbcd3a9c81%3A0xc277c47bb1bbcf43!2zQnVkYXBlc3QsIFTFsXpvbHTDsyB1LiA1MCwgMTA5NA!5e0!3m2!1shu!2shu!4v1546592503759" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
                  </div>
               </div>
            </div>
         </section>
      </div>
	 <footer class="footer-3">
         <div class="container">
            <div class="row">
		    <div class="col-md-12 col-lg-12 ">
				<div class="col-md-3 col-lg-3 footerbox"><br><p align="center"><a href="http://vermis.hu/" target="blank"><img src="https://vermis.hu/img/vermis_logo_footer.png"></a></p></div>
				<div class="col-md-3 col-lg-3 footerbox pad-vermis"><h3 class="footer-title"><b>A VERMIS-RŐL</b></h3><p class="vermis">2004 óta működő digitális csapatunk speciális szakértelemmel bír az earned media, vírusmarketing, gerilla marketing, a PR és az online PR, terjesztés, seeding, seo optimalizálás és komplex PPC menedzsment területeken.</p>
				
				<h3 class="footer-title">PROFILUNKRÓL</h3><p class="vermis">A Vermis Production egy online ügynökség. Tapasztalatunk kiterjed az online stratégiától a UX design-on át a social media feladatokig.</p></div>
				
				<div class="col-md-3 col-lg-3  footerbox pad-vermis"><h3 class="footer-title"><b>A NAGYKÖVETEKRŐL</b></h3><p class="vermis">Influencer programunk része, hogy megtaláljuk az adott márkát kedvelő, arról hitelesen nyilatkozó bloggert, aki a brandet képviselő, sok követőt megmozgató márkanagykövetté válik: tartalommal, eseményekkel, kreatív ötletekkel.</p>
				
				<h3 class="footer-title"><b>CONTANDME</b></h3><p class="vermis">Bloggerek, influencerek gyűjtőoldala, ahol a marketingdöntésekhez szükséges minden fontos információt, látogatottságot, elérést összegyűjtünk. Innen indul a sikeres influencer kampány.</p></div>
				
				<div class="col-md-3 col-lg-3  footerbox pad-vermis"><h3 class="footer-title"><b>KAPCSOLAT</b></h3>
				<p class="vermis"><b>Cím:</b><br>1096 Budapest, Thaly Kálmán u. 39. 1. emelet</p>
				<!--<p><b>Postacím:</b><br>1094 Budapest, Tűzoltó u. 50. 1. emelet</p>-->
				<p class="vermis"><b>Tel:</b><br><a href="tel:+36302227705" style="color: #707070;">+36 30 2227705</a></p>
				<p class="vermis"><b>Email:</b><br><a href="mailto:info@vermis.hu" style="color: #707070;">INFO@VERMIS.HU</a></p>
				</div>
            </div>
            </div>
         </div>
		 <div class="col-md-10"></div>
		 <div class="col-md-2"><a href="https://www.facebook.com/vermis.production/?fref=ts" target="_blank"><i class="fab fa-facebook-f fa-2x"></i></a>&nbsp;&nbsp;<a href="https://www.youtube.com/channel/UCkK_nDEmZ0gnEJ0mA5vJPVA" target="_blank"><i class="fab fa-youtube fa-2x"></i></a></div>
		 			<div class="col-md-12 line"></div>
		<div class="container">
			<div class="col-md-12 lheight"></div>
			<div class="col-md-12" align="center"><p class="vermis">© 2019 A V-Team Reklámügynökség Kft. A Geometry Global magyarországi ügynökségeinek partnere. Székhelye: 1096 Budapest, Thaly Kálmán u. 39.<br>
			<div class="col-md-12 lheight_2"></div><p class="vermis">A 01-09-288011 cégjegyzékszámon nyilvántartva, adószáma: 23186226-2-43</p>
			</div>
		</div>	
      </footer>
      
      <a href="#0" class="cd-top">Top</a>     
	  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5947aee2c31141e5"></script><!-- Go to www.addthis.com/dashboard to customize your tools --> 
      <script src="js/jquery.2.2.1.min.js"></script> 
      <script src="js/bootstrap.min.js"></script> 
      <script src="js/jquery.fittext.js"></script><!-- Slide Text --> 
      <script src="js/jquery.lettering.js"></script><!-- Slide Text --> 
      <script src="js/jquery.textillate.js"></script><!-- Slide Text --> 
      <script src="js/rlaccordion.min.js"></script><!-- Accordion --> 
      <script src="js/jquery.magnific-popup.min.js"></script><!-- Lightbox --> 
      <script src="js/jquery.appear.js"></script><!-- Progress Bar --> 
      <script src="js/waypoints.2.0.3.min.js"></script> <!--Counter --> 
      <script src="js/jquery.counterup.js"></script> <!--Counter --> 
      <script src="js/owl.carousel.min.js"></script><!-- Slide Images --> 
      <script src="js/isotope.pkgd.min.js"></script><!-- Gallery --> 
      <script src="js/jquery.downCount.js"></script> <!--Countdown --> 
      <script src="js/skrollr.min.js"></script><!-- Parallax --> 
      <script src="js/circle-progress.js"></script> <!--Progress Bar Circle --> 
      <script src="js/tabulous.js"></script><!-- Tabs --> 
      <script src="js/jquery.mb.YTPlayer.min.js"></script><!-- Video --> 
      <script src="js/jquery.flexslider-min.js"></script> <!--Slide Images --> 
      <script src="js/wow.min.js"></script><!-- Animation --> 
      <script src="js/validator.js"></script><!-- Validator Email Form --> 
      <script src="js/jarallax.min.js"></script><!-- Parallax --> 
      <script src="js/jarallax-video.min.js"></script><!-- Parallax Video --> 
      <script src="js/main.js"></script>
      <script>
	  var onMapMouseleaveHandler = function (event) {
    var that = $(this);

    that.on('click', onMapClickHandler);
    that.off('mouseleave', onMapMouseleaveHandler);
    that.find('iframe').css("pointer-events", "none");
  }

  var onMapClickHandler = function (event) {
    var that = $(this);

    // Disable the click handler until the user leaves the map area
    that.off('click', onMapClickHandler);

    // Enable scrolling zoom
    that.find('iframe').css("pointer-events", "auto");

    // Handle the mouse leave event
    that.on('mouseleave', onMapMouseleaveHandler);
  }

  // Enable map zooming with mouse scroll when the user clicks the map
  $('.maps.embed-container').on('click', onMapClickHandler);
	  </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-12066799-3', 'auto');
  ga('send', 'pageview');

</script>